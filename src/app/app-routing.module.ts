import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '', redirectTo: 'store', pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: 'store',
        loadChildren: () => import('./pages/store/store.module').then(m => m.StoreModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
