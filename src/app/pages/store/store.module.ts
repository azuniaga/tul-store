import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreComponent } from './store.component';
import { StoreRoutingModule } from './store-routing.module';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { ProductCardModule } from '@shared/components/product-card/product-card.module';
import { NavbarModule } from '@shared/components/navbar/navbar.module';

@NgModule({
  imports: [
    CommonModule,
    StoreRoutingModule,
    NavbarModule,
    NzGridModule,
    ProductCardModule
  ],
  declarations: [StoreComponent]
})
export class StoreModule { }
