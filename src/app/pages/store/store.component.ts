import { Component, OnInit } from '@angular/core';

import { ProductCartDto, ProductDto } from '@core/models';
import { OrderService } from '@core/services/order/order.service';
import { ProductsService } from '@core/services/products/products.service';
import { NotificationService } from '@core/services/notification/notification.service';
import { NotificationTypeEnum } from '@core/services/notification/enums/notification-type-enum';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {

  public productList: ProductDto[] = [];

  constructor(
    private orderService: OrderService,
    private productService: ProductsService,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit(): void {
    this.productService.getProducts.subscribe(products => this.productList = products);

  }

  addProduct(product: ProductCartDto): void {
    this.orderService.addProduct(product);
    this.notificationService.createMessage(NotificationTypeEnum.SUCCESS, 'Producto agregado');
  }

}
