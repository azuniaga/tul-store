import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { AccountService } from '@core/services/account/account.service';
import { UserDto } from '@core/models';
import { NotificationService } from '@core/services/notification/notification.service';
import { NotificationTypeEnum } from '@core/services/notification/enums/notification-type-enum';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public isRegister: boolean = false;
  public loggedUser: UserDto;
  public loginForm: FormGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(
    private accountService: AccountService,
    private notificationService: NotificationService
    ) {}

  ngOnInit(): void {
    this.accountService.loggedUser.subscribe((loggedUser: UserDto) => this.loggedUser = loggedUser);
  }

  submitForm(): void {
    const { email, password } = this.loginForm.value;
    if (!this.isRegister)
      this.accountService.login(email, password)
        .subscribe((ok: boolean) => {
          if (ok) this.notificationService.createMessage(NotificationTypeEnum.SUCCESS, 'Bienvenido!');
          else this.notificationService.createMessage(NotificationTypeEnum.ERROR, 'Error de usuario o constraseña');
        });
    else
      this.accountService.register(email, password)
        .subscribe((ok: boolean) => {
          if (ok) this.notificationService.createMessage(NotificationTypeEnum.SUCCESS, 'Bienvenido!');
          else this.notificationService.createMessage(NotificationTypeEnum.ERROR, 'Error de registro');
        });
  }

  signOut(): void {
    this.accountService.signOut();
  }

}
