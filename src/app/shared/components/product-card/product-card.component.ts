import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProductDto } from '@core/models';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() product: ProductDto;
  @Output() productAdded = new EventEmitter<ProductDto>();

  constructor() { }

  ngOnInit(): void {
  }

  addProduct(): void {
    this.productAdded.emit(this.product);
  }

}

