import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { LoginComponent } from '@shared/components/login/login.component';
import { CartComponent } from '@shared/components/cart/cart.component';
import { AccountService } from '@core/services/account/account.service';
import { UserDto } from '@core/models';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public modalConfig = {};
  public user: UserDto;

  constructor(
    private nzModal: NzModalService,
    private accountService: AccountService
  ) { }

  ngOnInit(): void {
    this.accountService.loggedUser.subscribe(user => {
      this.user = user;
      if (user) this.nzModal.closeAll();
    });
  }

  showLogin(): void {
    this.nzModal.create({
      nzContent: LoginComponent,
      nzFooter: null
    });
  }

  showCart(): void {
    this.nzModal.create({
      nzContent: CartComponent,
      nzFooter: null,
    });
  }
}
