import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NzMenuModule } from 'ng-zorro-antd/menu';
import { LoginModule } from '@shared/components/login/login.module';
import { CartModule } from '@shared/components/cart/cart.module';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NavbarComponent } from './navbar.component';

@NgModule({
  imports: [
    CommonModule,
    NzMenuModule,
    LoginModule,
    CartModule,
    NzModalModule,
    NzIconModule,
    NzBadgeModule
  ],
  declarations: [NavbarComponent],
  exports: [NavbarComponent]
})
export class NavbarModule { }
