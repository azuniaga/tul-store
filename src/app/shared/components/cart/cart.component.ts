import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { OrderService } from '@core/services/order/order.service';
import { NotificationService } from '@core/services/notification/notification.service';
import { NotificationTypeEnum } from '@core/services/notification/enums/notification-type-enum';
import { ProductCartDto } from '@core/models';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy {

  private subscriptions: Subscription;
  public products: ProductCartDto[];
  constructor(
    private orderService: OrderService,
    private notificationService: NotificationService
  ) { }

  ngOnInit(): void {
    this.subscriptions = this.orderService.cartProducts$.subscribe(products => this.products = products);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  addProduct(product: ProductCartDto): void {
    this.orderService.addProduct(product);
  }

  deleteProduct(product: ProductCartDto, deleteEntireProduct: boolean = false): void {
    this.orderService.deleteProduct(product, deleteEntireProduct);
    this.notificationService.createMessage(NotificationTypeEnum.SUCCESS, 'Producto eliminado');
  }

  completeOrder(): void {
    this.orderService.completeOrder()
      .subscribe(x => this.notificationService.createMessage(NotificationTypeEnum.SUCCESS, 'Orden Completada'));
  }

}
