import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import { ProductCardModule } from '@shared/components/product-card/product-card.module';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzGridModule } from 'ng-zorro-antd/grid';

@NgModule({
  imports: [
    CommonModule,
    ProductCardModule,
    NzListModule,
    NzIconModule,
    NzGridModule
  ],
  declarations: [CartComponent],
  exports: [CartComponent]
})
export class CartModule { }
