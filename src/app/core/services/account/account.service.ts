import { Injectable } from '@angular/core';
import { Observable,  from, BehaviorSubject, of } from 'rxjs';
import{ catchError, map } from 'rxjs/operators';
import { UserDto } from '@core/models/user-dto';

import firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  public loggedUser: BehaviorSubject<UserDto> = new BehaviorSubject(null);

  constructor(
    private angularFireAuth: AngularFireAuth,
  ) {
    this.angularFireAuth.user.subscribe(
      (user: firebase.User) => this.loggedUser.next(user ? { uid: user?.uid, email: user?.email } : null));
  }

  login(email: string, password: string): Observable<boolean> {
    return from(this.angularFireAuth.signInWithEmailAndPassword(email, password)).pipe(
      map(x => true), catchError(err => of(false)));
  }

  register(email: string, password: string): Observable<boolean> {
    return from(this.angularFireAuth.createUserWithEmailAndPassword(email, password)).pipe(
      map(x => true), catchError(err => of(false)));
  }

  signOut(): void {
    this.angularFireAuth.signOut();
  }

}
