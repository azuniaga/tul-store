import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ProductDto } from '@core/models/product-dto';

import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  public getProducts: BehaviorSubject<ProductDto[]> = new BehaviorSubject([]);

  constructor(private firestore: AngularFirestore) {
    this.firestore.collection('products').snapshotChanges().subscribe(items => {
      const products: ProductDto[] = items.map(a => a.payload.doc.data() as ProductDto);
      this.getProducts.next(products);
    });
  }
}
