import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription, from, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AngularFirestore, DocumentReference, QuerySnapshot } from '@angular/fire/firestore';
import { AccountService } from '@core/services/account/account.service';
import { ProductCartDto, CartDto, UserDto } from '@core/models';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private cartObs: Subscription;

  private cartDocumentId: string;
  private set cartProducts(products: ProductCartDto[]) { this.cartProducts$.next(products); }
  private get cartProducts(): ProductCartDto[] { return this.cartProducts$.value; }

  public cartProducts$: BehaviorSubject<ProductCartDto[]> = new BehaviorSubject([]);

  constructor(
    private firestore: AngularFirestore,
    private accountService: AccountService
  ) {
    this.accountService.loggedUser.subscribe((user: UserDto) => {
      if (user) this.getUserCart(user.uid);
      else this.cartProducts = [];
    });
  }

  getUserCart(user_id: string): void {
    this.firestore.collection<CartDto>(
      'carts', ref => ref.where('user_id', '==', user_id).where('status', '==', 'pending')
    ).get().subscribe((snapshot: QuerySnapshot<CartDto>) => {
      if (snapshot.empty) {
        this.createCart(this.cartProducts, user_id).subscribe((doc: DocumentReference<CartDto>) => {
          this.cartDocumentId = doc.id;
          this.subscribeCartChanges();
        });
      } else {
        this.cartDocumentId = snapshot.docs[0].id;
        this.subscribeCartChanges();
      }
    });
  }

  createCart(products: ProductCartDto[], user_id?: string): Observable<DocumentReference<CartDto>> {
    return from(this.firestore.collection<CartDto>('carts').add({ user_id: user_id, products: products, status: 'pending' }));
  }

  updateProductsCart(products: ProductCartDto[]): Observable<void> {
    if (this.cartDocumentId)
      return from(this.firestore.collection<CartDto>('carts').doc(this.cartDocumentId).update({ products: products }));
  }

  subscribeCartChanges(): void {
    if (!this.cartObs && this.cartDocumentId)
      this.cartObs = this.firestore.collection<CartDto>('carts').doc(this.cartDocumentId).valueChanges()
        .subscribe((cart: CartDto) => this.cartProducts = [...cart.products]);
  }

  addProduct(product: ProductCartDto): void {
    const isAdded = this.cartProducts.find(item => item.id === product.id);
    if (isAdded) {
      isAdded.qty++;
    } else {
      product.qty = 1;
      this.cartProducts.push({...product});
    }
    this.updateProductsCart(this.cartProducts).subscribe();
  }

  deleteProduct(product: ProductCartDto, deleteEntireProduct: boolean = false): void {
    product.qty--;
    if (deleteEntireProduct || product.qty === 0) {
      this.cartProducts = this.cartProducts.filter(x => x.id !== product.id);
    } else {
      this.cartProducts = [...this.cartProducts.map(p => {
        return {
          ...p,
          qty: p.id === product.id ? product.qty : p.qty
        }
      })]
    }
    this.updateProductsCart(this.cartProducts).subscribe();
  }

  completeOrder(): Observable<void> {
    return from(this.firestore.collection<CartDto>('carts').doc(this.cartDocumentId).update({ status: 'completed' }))
      .pipe(tap(x =>{
        this.cartObs.unsubscribe();
        this.cartObs = null;
        this.cartProducts = [];
        this.getUserCart(this.accountService.loggedUser.value.uid);
      }));
  }

}
