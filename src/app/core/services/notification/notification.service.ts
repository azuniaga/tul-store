import { Injectable } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';

import { NotificationTypeEnum } from './enums/notification-type-enum';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private notification: NzNotificationService) { }

  createMessage(type: NotificationTypeEnum, message: string): void {
    this.notification.create(type, message, '',{ nzPlacement: 'bottomRight' });
  }

}
