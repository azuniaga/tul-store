import { ProductDto } from "@core/models";

export const PRODUCTS: ProductDto[] = [
  {
    id: 1, nombre: 'Producto 1', sku: 'sku-producto-1', descripcion: 'Lorem descripcion de producto', precio: 300
  },
  {
    id: 2, nombre: 'Producto 2', sku: 'sku-producto-2', descripcion: 'Lorem descripcion de producto', precio: 800
  },
  {
    id: 3, nombre: 'Producto 3', sku: 'sku-producto-3', descripcion: 'Lorem descripcion de producto', precio: 900
  },
  {
    id: 4, nombre: 'Producto 4', sku: 'sku-producto-4', descripcion: 'Lorem descripcion de producto', precio: 300
  },
  {
    id: 5, nombre: 'Producto 5', sku: 'sku-producto-5', descripcion: 'Lorem descripcion de producto', precio: 500
  },
  {
    id: 6, nombre: 'Producto 6', sku: 'sku-producto-6', descripcion: 'Lorem descripcion de producto', precio: 700
  },
  {
    id: 7, nombre: 'Producto 7', sku: 'sku-producto-7', descripcion: 'Lorem descripcion de producto', precio: 1000
  },
  {
    id: 8, nombre: 'Producto 8', sku: 'sku-producto-8', descripcion: 'Lorem descripcion de producto', precio: 200
  }
];
