export interface ProductDto {
  id: number;
  nombre: string;
  sku: string;
  descripcion: string;
}
