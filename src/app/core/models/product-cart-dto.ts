import { ProductDto } from '@core/models';

export interface ProductCartDto extends ProductDto {
  qty?: number | 1;
}
