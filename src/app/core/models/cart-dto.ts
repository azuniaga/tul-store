import { ProductCartDto } from './product-cart-dto';

export interface CartDto {
  user_id: string;
  status: 'pending' | 'completed';
  products: ProductCartDto[];
}
