export interface UserDto {
  uid: string;
  email: string;
}
