// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC28v2hIitDZh_VUwqMISjE0uO6ZnY6kEw",
    authDomain: "tul-store-87d22.firebaseapp.com",
    projectId: "tul-store-87d22",
    storageBucket: "tul-store-87d22.appspot.com",
    messagingSenderId: "201602942723",
    appId: "1:201602942723:web:3bdf7d2dfc3a0c3e474b36",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
